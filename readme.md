﻿# CAR-NET Backend

API creada para consumir los servicios de registro de alumnos, en actividades culturales de la Facultad de Ingeniería, de la Universidad Autónoma de Baja California.

## Primeros pasos

* Clonar el repositorio que se encuentra en: [CAR-NET Backend](https://bitbucket.org/Moises001/car-net-backend/src/master/)


### Prerequisitos
* [PHP 7.2](http://php.net/downloads.php)
* [Composer](https://getcomposer.org/)
* [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)

### Instalación

* Ubicarse en el directorio donde se encuentra el repositorio y ejecutar el siguiente comando:
```
composer install
```

* Instalar Homestead con: 

```
vendor/bin/homestead make
```

* Levantar la maquina virtual de vagrant: 

```
vagrant up
```

* Una vez arriba la maquina virtual ir a la direccion: 192.168.10.10 (para localhost)

![Alt text](images/laravel-carnet.png)

Si al ir a esa dirección no aparece algo como la imagen anterior, ejecute el siguiente comando e intente de nuevo

```
vagrant up --provision
```

* Una vez completado el paso anterior, siga los siguientes pasos para crear los roles:
```
vagrant ssh
```
Una vez hecha la conexión
```
//Ir a la carpeta code
vagrant@car-net-backend:~$ cd code
vagrant@car-net-backend:~/code$ php artisan permission:create-role Alumno
vagrant@car-net-backend:~/code$ php artisan permission:create-role Encargado
vagrant@car-net-backend:~/code$ php artisan permission:create-role Administrador
```

## Configuración inicial
Es necesario cambiar el nombre del archivo "example.env" el cual esta ubicado en la raíz del proyecto. El nombre del archivo debe ser ".env".
* Ejecutar los siguientes comandos:
```
php artisan config:clear
php artisan key:generate
```

## Despliegue

Para realizar el despliegue, es necesario cambiar las rutas en dos metodos de QRController: obtenerQREvento y obtenerQRAlumno. 
```
 $local_url="tu_ruta_de_despliegue/EV-".$id.".svg";
```
## Documentación del API
![Alt text](images/api_evento.png)
![Alt text](images/api_qr.png)
![Alt text](images/api_user.png)

Para mas información sobre las respuestas de nuestro API visita nuestra pagina en [Swagger](https://app.swaggerhub.com/apis/CAR-NET/car-net/1.0.0).
## Construido con

* [Laravel Homestead](https://laravel.com/docs/5.6/homestead) - Ambiente de desarrollo
* [Composer](https://getcomposer.org/) - Manejador de dependencias
* [Spatie](https://github.com/spatie/laravel-permission) - Usado para manejar roles y permisos
* [Laravel QR Code Generator](https://github.com/werneckbh/laravel-qr-code) - Usado para la creación de códigos QR

## Versionamiento

Usamos [Git](https://git-scm.com/) para versionamiento. Para las versiones disponibles, ir a [CAR-NET Backend](https://bitbucket.org/Moises001/car-net-backend/). 

## Autores

* **Moises Sandoval Contreras** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Saúl Daniel Silva García** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Evelyn Hull Valdez** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Jose Adrian Palomares Luevano** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Octavio Eduardo Hernandez Sixto** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Cesar Gonzalez Barreras** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)


## Agradecimientos

* Luis Enrique Vizcarra Corral - Sponsor del proyecto
* Jose Martín Olguín Espinoza - Supervisor del proyecto
