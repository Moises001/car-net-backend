<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('listarEventos','EventoController@listarEventos');
Route::get('consultarEvento/{evento}','EventoController@consultarEvento');
Route::post('registrarAlumno','EventoController@registrarAlumno');
Route::post('agregarEvento','EventoController@agregarEvento');
Route::get('QrEvento/{id}','EventoController@QrEvento');
Route::get('crearEventosPrueba','EventoController@crearEventosPrueba');
Route::get('crearUsuarioPrueba','UserController@crearUsuarioPrueba');
Route::get('listarUsuarios','UserController@listarUsuarios');
Route::delete('eliminarUsuario/{user}','UserController@eliminarUsuario');
Route::get('consultarCarnetsUsuario/{user}','UserController@consultarCarnetsUsuario');
Route::get('registrarAlumnoDummie','EventoController@registrarAlumnoDummie');
Route::get('obtenerQREvento/{id}','Qrcontroller@obtenerQREvento');
Route::get('obtenerQRAlumno/{id}','Qrcontroller@obtenerQRAlumno');
Route::get('devolverImagen/{id}','QRController@devolverImagen');
Route::get('obtenerSellos/{id}','UserController@obtenerSellos');
Route::get('infoUsuario/{id}','UserController@infoUsuario');
Route::get('download/{file}' , 'EventoController@downloadFile');
Route::get('downloadFileAlumno/{file}' , 'EventoController@downloadFileAlumno');
Route::get('obtenerQRAlumno/{file}' , 'QRController@obtenerQRAlumno');
Route::get('consultarUsuario/{id}','UserController@infoUsuario');
