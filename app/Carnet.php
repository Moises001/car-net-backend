<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carnet extends Model
{
    protected $fillable = [
        'user_id'
    ];

    protected $table = 'carnets';

    public function sellos(){
        return $this->hasMany('App\Sello');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
