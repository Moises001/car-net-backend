<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable =[
        'direccion',
        'nombre',
        'categoria',
        'organizador',
        'horario',
        'duracion',
        'num_sellos',
        'activo',
        'qr',
    ];

    protected $table = 'eventos';

    public function sellos(){
        return $this->hasMany('App\Sello');
    }

}
