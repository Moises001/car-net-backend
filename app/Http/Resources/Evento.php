<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Evento extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'direccion'=> $this->direccion,
            'nombre'=> $this->nombre,
            'categoria'=>$this->categoria,
            'organizador'=>$this->organizador,
            'horario'=> $this->horario,
            'duracion' => $this->duracion,
            'num_sellos' => $this->num_sellos,
            'activo'=>$this->activo,
            'qr' => url("/api/evento/$this->id"),
            'self'=>url("/api/usuario/{$this->id}")
        ];
    }
}
