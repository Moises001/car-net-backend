<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Sello extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'evento_id'=> $this->evento_id,
            'carnet_id' => $this->carnet_id,
            'self'=>url("/api/usuario/{$this->id}")
        ];
    }
}
