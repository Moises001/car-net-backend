<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Qrcontroller extends Controller
{
	//FUNCIONES DE EJEMPLO

    public function generarQRasPng($info){
    	$file=public_path($info.'.png');
    	return \QRCode::text($info)->setOutfile($file)->png();
    }

	public function generarQRasSvg($info){
    	$file=public_path($info.'.png');
    	return \QRCode::text($info)
                ->setErrorCorrectionLevel('H')
                ->setSize(4)
                ->setMargin(2)
                ->svg();
    }


    public function QrAlumno($id){
    	$file=public_path('AL-'.$id.'.png');
    	\QRCode::text('AL-'.$id)->setOutfile($file)->png();
    	return \QRCode::text('AL-'.$id)
                ->setErrorCorrectionLevel('H')
                ->setSize(4)
                ->setMargin(2)
                ->svg();
    }

     public function QrEvento($id){
    	$file=public_path('EV-'.$id.'.png');
    	\QRCode::text('EV-'.$id)->setOutfile($file)->png();
    	return \QRCode::text('EV-'.$id)
                ->setErrorCorrectionLevel('H')
                ->setSize(4)
                ->setMargin(2)
                ->svg();
    }

    public function obtenerQREvento($id){
        //ESTA RUTA DEBE CAMBIARSE EN EL DESPLIEGUE DE LA APP
        $local_url="http://192.168.10.10/EV-".$id.".svg";
        return $local_url;
    }

    public function obtenerQRAlumno($id){
        //ESTA RUTA DEBE CAMBIARSE EN EL DESPLIEGUE DE LA APP
        $local_url="http://192.168.10.10/AL-".$id.".svg";
        return $local_url;
    }
    public function devolverImagen($id){
            $response=public_path('EV-'.$id.'.svg');
        return response()->file($response);

    }

}
