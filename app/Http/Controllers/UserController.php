<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Carnet;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Sello as SelloResource;
use App\Http\Resources\Carnet as CarnetResource;
class UserController extends Controller
{
    public function agregarUsuario(Request $request){
        $request->validate(['name'=>'required|string','email'=>'required|string|email|unique:users']);
        $usuario=new User(['name'=>$request->nombre,'email'=>$request->email]);

        $usuario->assignRole('Alumno');
        try{
            $usuario->save();
            $carnet=new Carnet(['user_id'=>$usuario->id]);
            $carnet->save();
            $file=public_path('AL-'.$usuario->id.'.svg');
            $filePng=public_path('AL-'.$usuario->id.'.png');
            \QRCode::text('AL-'.$usuario->id)->setOutfile($file)->svg();
            \QRCode::text('AL-'.$usuario->id)->setOutfile($filePng)->png();
        }catch(\Exception $e){
            return response()->json($e->getMessage,500);
        }
        return response()->json(['message'=>'Usuario registrado exitosamente','link'=>url('/api/users/'.$usuario->id)]);
    }

    public function crearUsuarioPrueba(){
        $usuario=new User(['name'=>'David Said Ramirez','email'=>'david.ramirez@uabc.edu.mx']);
        $usuario->assignRole('Alumno');
        try{
            $usuario->save();
            $carnet=new Carnet(['user_id'=>$usuario->id]);
            $carnet->save();
            $file=public_path('AL-'.$usuario->id.'.svg');
            $filePng=public_path('AL-'.$usuario->id.'.png');
            \QRCode::text('AL-'.$usuario->id)->setOutfile($file)->svg();
            \QRCode::text('AL-'.$usuario->id)->setOutfile($filePng)->png();
        }catch(\Exception $e){
            return response()->json($e,500);
        }
        return response()->json(['message'=>'Usuario registrado exitosamente','link'=>url('/web/users/'.$usuario->id)]);
    }

    public function obtenerSellos($id){
        $usuario=User::find($id);
        if(!$usuario){
            return response()->json(['message'=>'Usuario no encontrado'],404);
        }
        if($usuario->carnets->isEmpty()){
            return response()->json(['message'=>'No se encontraron carnets asignados al usuario'],404);
        }
        $carnets=$usuario->carnets;
        $carnet=$carnets->last();
        $sellos=SelloResource::collection($carnet->sellos);
        return response()->json($sellos,200);
    }


    public function listarUsuarios(){
        $usuarios=User::all();

        if($usuarios->isEmpty()){
            return response()->json(['message'=>'Sin registros'],404);
        }
        $coleccionUsuarios=UserResource::collection($usuarios);

        return response()->json($coleccionUsuarios,200);
    }

    public function infoUsuario($user){
        $usuario=User::find($user);
        if(!$usuario){
            return response()->json(['message'=>'Usuario no encontrado'],404);
        }
        return response()->json($usuario);
    }

    public function consultarCarnetsUsuario($user){
        $usuario=User::find($user);
        if(!$usuario){
            return response()->json(['message'=>'Usuario no encontrado'],404);
        }
        if($usuario->carnets->isEmpty()){
            return response()->json(['message'=>'No se encontraron carnets asignados al usuario'],404);
        }
        $coleccionCarnetsUsuario=CarnetResource::collection($usuario->carnet);
        return response()->json($coleccionCarnetsUsuario,200);
    }

    public function eliminarUsuario($user){
        $usuario=User::find($user);
        if(!$usuario){
            return response()->json(['message'=>'Usuario no encontrado'],404);
        }
        try{
            $usuario->delete();
        }catch(\Exception $e){
            return response()->json($e->getMessage(),500);
        }
        return response()->json(['message'=>'Usuario eliminado exitosamente'],200);
    }
}
