<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Evento;
use App\User;
use App\Sello;
use App\Carnet;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Evento as EventoResource;
use App\Http\Resources\Carnet as CarnetResource;
use App\Http\Resources\Sello as SelloResource;
class EventoController extends Controller
{
	public function listarEventos(){
	    	$eventos=Evento::all();
	    	if($eventos->isEmpty()){
	    		return response()->json(['message'=>'Sin registros'],404);
	    	}
	    	$coleccionEventos=EventoResource::collection($eventos);
	    	return response()->json($coleccionEventos,200);
	    }
 	public function consultarEvento($evento){
    	if(!Evento::find($evento)){
    		return response()->json(['message'=>'Sin registros'],404);
    	}
    	return response()->json(new EventoResource(Evento::find($evento)),200);
    }


    public function registrarAlumno(Request $request){
	    $evId=$request->eventoId;
	    $alId=$request->alumnoId;
        //ENCONTRAR ALUMNO Y EVENTO EN LA BD
        $alumno=User::find($alId);
        $evento=Evento::find($evId);
        //TRAER CARNETS DE ALUMNO
        $carnets=CarnetResource::collection($alumno->carnets);
        //TRAER ULTIMO CARNET
        $carnet=$carnets->last();
        $carnetbd=Carnet::find($carnet->id);
        $sellos=SelloResource::collection($carnetbd->sellos);
        for($i=0;$i<$sellos->count();$i++){
            if($sellos[$i]->evento_id==$evId){
                return response()->json(['message'=>'EVENTO YA EXISTENTE']);
            }
        }
        if(($sellos->count()+$evento->num_sellos)>8){
            $dif=8-$sellos->count();

            for( $i=0; $i<$dif;$i++){
                $sello=new Sello(['carnet_id'=>$carnet->id,'evento_id'=>$evId]);
                try{
                    $sello->save();
                }catch(\Exception $e){
                    return response()->json($e->getMessage,500);
                }
            }
            $carnetNuevo=new Carnet(['user_id'=>$alId]);
            $carnetNuevo->save();
            for( $i=0; $i<($sellos->count()-$dif);$i++){
                $sello=new Sello(['carnet_id'=>$carnetNuevo->id,'evento_id'=>$evId]);
                try{
                    $sello->save();
                }catch(\Exception $e){
                    return response()->json($e->getMessage,500);
                }
            }

        }else{
            //CREACION DE SELLOS
            for( $i=0; $i<$evento->num_sellos;$i++){
                $sello=new Sello(['carnet_id'=>$carnet->id,'evento_id'=>$evId]);
                try{
                    $sello->save();
                }catch(\Exception $e){
                    return response()->json($e,500);
                }
            }

        }
        return response()->json(['message'=>'Alumno registrado exitosamente en evento']);
    }

    public function registrarAlumnoDummie(){
        $evId=1;
        $alId=1;
        //ENCONTRAR ALUMNO Y EVENTO EN LA BD
        $alumno=User::find($alId);
        $evento=Evento::find($evId);
        //TRAER CARNETS DE ALUMNO
        $carnets=CarnetResource::collection($alumno->carnets);
        //TRAER ULTIMO CARNET
        $carnet=$carnets->last();
        $carnetbd=Carnet::find($carnet->id);
        $sellos=SelloResource::collection($carnetbd->sellos);
        for($i=0;$i<$sellos->count();$i++){
            if($sellos[$i]->evento_id==$evId){
                return response()->json(['message'=>'EVENTO YA EXISTENTE']);
            }
        }
        if(($sellos->count()+$evento->num_sellos)>8){
            $dif=8-$sellos->count();

            for( $i=0; $i<$dif;$i++){
                $sello=new Sello(['carnet_id'=>$carnet->id,'evento_id'=>$evId]);
                try{
                    $sello->save();
                }catch(\Exception $e){
                    return response()->json($e->getMessage,500);
                }
            }
            $carnetNuevo=new Carnet(['user_id'=>$alId]);
            $carnetNuevo->save();
            for( $i=0; $i<($sellos->count()-$dif);$i++){
                $sello=new Sello(['carnet_id'=>$carnetNuevo->id,'evento_id'=>$evId]);
                try{
                    $sello->save();
                }catch(\Exception $e){
                    return response()->json($e->getMessage,500);
                }
            }
            return response()->json(['message'=>'Alumno registrado exitosamente en evento']);
        }else{
        //CREACION DE SELLOS
        for( $i=0; $i<$evento->num_sellos;$i++){
            $sello=new Sello(['carnet_id'=>$carnet->id,'evento_id'=>$evId]);
            try{
                $sello->save();
            }catch(\Exception $e){
                return response()->json($e,500);
            }
        }
    }
    }


public function crearEvento(Request $request){
        $request->validate(['direccion'=>'required|string','nombre'=>'required|string','categoria'=>'string','organizador'=>'string','horario'=>'required|string','duracion'=>'required|string','num_sellos'=>'required|string']);
        $evento=new Evento(['direccion'=>$request->direccion,'nombre'=>$request->nombre,'categoria'=>$request->categoria,'organizador'=>$request->organizador,'horario'=>$request->horario,'duracion'=>$request->duracion,'num_sellos'=>$request->num_sellos]);
        try{
            $evento->save();
            $evento->qr=QrEvento($evento->id);
            $evento->save();
        }catch(\Exception $e){
            return response()->json($e->getMessage,500);
        }
        return response()->json(['message'=>'Evento registrado exitosamente','link'=>url('/api/consultarEvento/'.$evento->id)]);
    }

    public function crearEventosPrueba(){
        $d=strtotime("10:30pm April 15 2014");
    	 $evento=new Evento(['direccion'=>'UABC','horario'=>date("Y-m-d"),'nombre'=>'Conferencia de ciencia','categoria'=>'Cultural','organizador'=>'Adolfo Ruelas','duracion'=>'3 dias','num_sellos'=>'2']);
    	  try{
            $evento->save();
         //$evento->qr=QrEvento($evento->id);
            $file=public_path('EV-'.$evento->id.'.svg');
    	    \QRCode::text('EV-'.$evento->id)->setOutfile($file)->svg();
              $filePng=public_path('EV-'.$evento->id.'.png');
              \QRCode::text('EV-'.$evento->id)->setOutfile($filePng)->png();
    	    $evento->qr=$file;
            $evento->save();
        }catch(\Exception $e){
            return response()->json($e,500);
        }
        return response()->json(['message'=>'Evento registrado exitosamente','link'=>url($file)]);
    }
     public function QrEvento($id){
    	$file=public_path('EV-'.$id.'.svg');
    	 \QRCode::text('EV-'.$id)->setOutfile($file)->svg();
         $filePng=public_path('EV-'.$id.'.png');
         \QRCode::text('EV-'.$id)->setOutfile($filePng)->png();
    	 return $file;
    }

    public function downloadFile($file){
        $pathtoFile = public_path().'/EV-'.$file.'.png';
        return response()->download($pathtoFile);
      }

    public function downloadFileAlumno($file){
        $pathtoFile = public_path().'/AL-'.$file.'.png';
        return response()->download($pathtoFile);
    }
}
