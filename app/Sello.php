<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sello extends Model
{
    protected $fillable =[
        'evento_id',
        'carnet_id'
    ];

    protected $table = 'sellos';

    public function evento(){
        return $this->belongsTo('App\Evento');
    }

    public function carnet(){
        return $this->belongsTo('App\Carnet');
    }
}
